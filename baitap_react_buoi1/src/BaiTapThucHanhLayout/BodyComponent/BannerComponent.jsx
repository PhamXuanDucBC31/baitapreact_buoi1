import React, { Component } from "react";

export default class BannerComponent extends Component {
  render() {
    return (
      <div className="container py-4">
        <h1>A Warm Wellcome</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi quam dicta officia aliquam qui velit libero quis voluptas quibusdam! Qui voluptatibus dolor aliquid harum dolorem soluta earum eaque veritatis facilis?</p>
        <button className="btn btn-primary">Call to action!</button>
      </div>
    );
  }
}
