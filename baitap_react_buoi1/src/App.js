import logo from "./logo.svg";
import "./App.css";
import HeaderComponent from "./BaiTapThucHanhLayout/HeaderComponent";
import BannerComponent from "./BaiTapThucHanhLayout/BodyComponent/BannerComponent";
import ItemComponent from "./BaiTapThucHanhLayout/BodyComponent/ItemComponent";
import FooterComponent from "./BaiTapThucHanhLayout/FooterComponent";

function App() {
  return (
    <div className="App">
      <HeaderComponent />
      <BannerComponent />
      <ItemComponent />
      <FooterComponent />
    </div>
  );
}

export default App;
